from flask import Flask, render_template, request
from werkzeug.utils import secure_filename

from src import answer_card_recognition

app = Flask(__name__, template_folder='../template')


@app.route('/', methods=['POST', 'GET'])
def index():
    return render_template("hello.html")


@app.route('/result', methods=['POST'])
def show_result():
    if request.method == 'POST':
        f = request.files['profile_pic']
        path = '../upload_pic/' + secure_filename(f.filename)
        f.save(path)
        exam_num, selected_choices = answer_card_recognition.answer_card_recognition(path)
        return render_template('result.html', exam_num=exam_num, selected_choices=selected_choices)
    else:
        return render_template('hello.html', message='图片上传失败')


if __name__ == "__main__":
    app.run(debug=True)
