import os
import pandas as pd

import utils
from src import answer_card_recognition

if __name__ == '__main__':
    utils.get_selected_box_size()
    path_dir = "../answer_card"
    names = os.listdir(path_dir)
    # random.shuffle(names)
    # names = ["SKM_C36818012910450_0055.jpg"]
    col = ["path", "ID"]
    for i in range(1, 51):
        num = "Q" + chr(i + ord('0'))
        col.append(num)
    result = []
    for name in names:
        item = []
        utils.read_cnt()
        path = path_dir + "/" + name
        item.append(path)
        print(path)
        exam_num, answers = answer_card_recognition.answer_card_recognition(path)
        item.append(exam_num)
        for key in range(1, 51):
            if key in answers.keys():
                answers_item = answers[key]
                one_answer = ""
                for a in answers_item:
                    if one_answer != "":
                        one_answer += "|"
                    one_answer += a
                item.append(one_answer)
            else:
                item.append("")
        utils.save_cnt()
        print(item)
        result.append(item)
        break
    df = pd.DataFrame(result, columns=col)
    df.to_csv("../result.csv", encoding='utf-8')
